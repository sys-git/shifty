# -*- coding: utf-8 -*-
"""
Test shifty: wrap.
"""

import unittest

from shifty.core import LEFT, RIGHT
from shifty.wrappers import wrap_right, wrap_left, shift, wrap
from shifty.impls import shifty


class test_wrap_left(unittest.TestCase):
    def setUp(self):
        self.a = [1, 2, 3, 4]

    def test_left(self):
        b = wrap_left(self.a, 3, in_place=False)
        assert b == [4, 1, 2, 3], b
        assert self.a is not b

    def test_left_in_place(self):
        b = wrap_left(self.a, 2, in_place=True)
        assert self.a is b
        assert b == [3, 4, 1, 2], b

    def test_left_modulo(self):
        b = wrap_left(self.a, 5, in_place=False)
        assert b == [2, 3, 4, 1], b
        c = wrap_left(self.a, 1)
        assert c == b

    def test_left_modulo_in_place(self):
        b = wrap_left(self.a, 5, in_place=True)
        assert self.a is b
        assert b == [2, 3, 4, 1], b
        c = wrap_left(self.a, 1, in_place=True)
        assert self.a is c
        assert b == c


class test_wrap_right(unittest.TestCase):
    def setUp(self):
        self.a = [1, 2, 3, 4]

    def test_right(self):
        b = wrap_right(self.a, 3, in_place=False)
        assert b == [2, 3, 4, 1], b
        assert self.a is not b

    def test_right_in_place(self):
        b = wrap_right(self.a, 2, in_place=True)
        assert self.a is b
        assert b == [3, 4, 1, 2], b

    def test_right_modulo(self):
        b = wrap_right(self.a, 5, in_place=False)
        assert b == [4, 1, 2, 3], b
        c = wrap_right(self.a, 1)
        assert c == b

    def test_right_modulo_in_place(self):
        b = wrap_right(self.a, 5, in_place=True)
        assert self.a is b
        assert b == [4, 1, 2, 3], b
        c = wrap_right(self.a, 1, in_place=True)
        assert self.a is c
        assert b == c


class test_shift_shifty_right(unittest.TestCase):
    def setUp(self):
        self.a = shifty([1, 2, 3, 4])

    def test_right(self):
        b = self.a.wrap_right(3, in_place=False)
        assert b == [2, 3, 4, 1], b
        assert self.a is not b

    def test_right_in_place(self):
        b = self.a.wrap_right(2, in_place=True)
        assert self.a is b
        assert b == [3, 4, 1, 2], b

    def test_right_modulo(self):
        b = self.a.wrap_right(5)
        assert b == [4, 1, 2, 3], b
        c = self.a.wrap_right(1)
        assert c == b

    def test_right_modulo_in_place(self):
        b = self.a.wrap_right(5, in_place=True)
        assert self.a is b
        assert b == [4, 1, 2, 3], b
        c = self.a.wrap_right(1, in_place=True)
        assert self.a is c
        assert b == c


class test_shift_shifty_left(unittest.TestCase):
    def setUp(self):
        self.a = shifty([1, 2, 3, 4])

    def test_left(self):
        b = self.a.wrap_left(3, in_place=False)
        assert b == [4, 1, 2, 3], b
        assert self.a is not b

    def test_left_in_place(self):
        b = self.a.wrap_left(2, in_place=True)
        assert self.a == b
        assert b == [3, 4, 1, 2], b

    def test_left_modulo(self):
        b = self.a.wrap_left(5)
        assert b == [2, 3, 4, 1], b
        c = self.a.wrap_left(1)
        assert c == b

    def test_left_modulo_in_place(self):
        b = self.a.wrap_left(5, in_place=True)
        assert self.a is b
        assert b == [2, 3, 4, 1], b
        c = self.a.wrap_left(1, in_place=True)
        assert self.a is c
        assert b == c


class test_invalid_direction(unittest.TestCase):
    def setUp(self):
        self.a = [1, 2, 3, 4]

    def test_shift(self):
        self.assertRaises(ValueError, lambda: shift(self.a, 3, direction=None))
        self.assertRaises(ValueError, lambda: shift(self.a, 3, direction='a'))

    def test_wrap(self):
        self.assertRaises(ValueError, lambda: wrap(self.a, 3, direction=None))
        self.assertRaises(ValueError, lambda: wrap(self.a, 3, direction='a'))


class test_shifty_iter_inplace(unittest.TestCase):
    def setUp(self, in_place=True):
        self.a = shifty([1, 2, 3, 4])
        self.in_place = in_place

    def test_left_one(self):
        iter_ = self.a.iter(1, LEFT, in_place=self.in_place)
        v = iter_.next()
        assert v == [2, 3, 4, 1], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [3, 4, 1, 2], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [4, 1, 2, 3], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [1, 2, 3, 4], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [2, 3, 4, 1], v
        if self.in_place:
            assert self.a is v

    def test_left_four(self):
        iter_ = self.a.iter(4, LEFT, in_place=self.in_place)
        v = iter_.next()
        assert v == [1, 2, 3, 4], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [1, 2, 3, 4], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [1, 2, 3, 4], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [1, 2, 3, 4], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [1, 2, 3, 4], v
        if self.in_place:
            assert self.a is v

    def test_right_one(self):
        iter_ = self.a.iter(1, RIGHT, in_place=self.in_place)
        v = iter_.next()
        assert v == [4, 1, 2, 3], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [3, 4, 1, 2], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [2, 3, 4, 1], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [1, 2, 3, 4], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [4, 1, 2, 3], v
        if self.in_place:
            assert self.a is v

    def test_right_four(self):
        iter_ = self.a.iter(4, RIGHT, in_place=self.in_place)
        v = iter_.next()
        assert v == [1, 2, 3, 4], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [1, 2, 3, 4], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [1, 2, 3, 4], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [1, 2, 3, 4], v
        if self.in_place:
            assert self.a is v
        v = iter_.next()
        assert v == [1, 2, 3, 4], v
        if self.in_place:
            assert self.a is v


class test_shifty_iter_notinplace(test_shifty_iter_inplace):
    def setUp(self):
        test_shifty_iter_inplace.setUp(self, in_place=False)


class test_shifty_itervalue_inplace(unittest.TestCase):
    def setUp(self, in_place=True):
        self.a = shifty([1, 2, 3, 4])
        self.in_place = in_place

    def test_left_one(self):
        iter_ = self.a.iterindex(0, 1, LEFT, in_place=self.in_place)
        v = iter_.next()
        assert v == 2
        v = iter_.next()
        assert v == 3
        v = iter_.next()
        assert v == 4
        v = iter_.next()
        assert v == 1

    def test_left_four(self):
        iter_ = self.a.iterindex(2, 4, LEFT, in_place=self.in_place)
        v = iter_.next()
        assert v == 3
        v = iter_.next()
        assert v == 3
        v = iter_.next()
        assert v == 3
        v = iter_.next()
        assert v == 3

    def test_right_two(self):
        iter_ = self.a.iterindex(1, 2, RIGHT, in_place=self.in_place)
        v = iter_.next()
        assert v == 4
        v = iter_.next()
        assert v == 2
        v = iter_.next()
        assert v == 4
        v = iter_.next()
        assert v == 2

    def test_right_three(self):
        iter_ = self.a.iterindex(3, 3, RIGHT, in_place=self.in_place)
        v = iter_.next()
        assert v == 1
        v = iter_.next()
        assert v == 2
        v = iter_.next()
        assert v == 3
        v = iter_.next()
        assert v == 4


class test_shifty_itervalue_notinplace(test_shifty_itervalue_inplace):
    def setUp(self):
        test_shifty_itervalue_inplace.setUp(self, in_place=False)


if __name__ == '__main__':  # pragma no cover
    pass

