# -*- coding: utf-8 -*-
"""
Test utils.
"""

import unittest

from shifty.utils import xiter, new_class, clear_instance
from shifty.impls import shifty


class test_invert_iter(unittest.TestCase):
    def test_first_index_gets_through(self):
        result = []
        for count, i in enumerate(xiter(iter([1, 2, 3, 4]))):
            if count == 4:
                break
            result.append(i)
        self.assertEquals(result, [0, 5, 6, 7])

    def test_first_index_cant_get_through(self):
        result = []
        for count, i in enumerate(xiter(iter([0, 1, 2, 3, 4]))):
            if count == 4:
                break
            result.append(i)
        self.assertEquals(result, [5, 6, 7, 8])

    def test_gaps(self):
        result = []
        for count, i in enumerate(xiter(iter([0, 3, 8, 9, 15]))):
            if i == 17:
                break
            result.append(i)
        self.assertEquals(result, [1, 2, 4, 5, 6, 7, 10, 11, 12, 13, 14, 16])

    def test_non_integer_start(self):
        starts = [float(0), 'blah', object()]
        for s in starts:
            self.assertRaises(TypeError,
                              lambda: [xiter(iter([1, 2, 3, 4]), s)])

    def test_negative_start(self):
        self.assertRaises(ValueError, lambda: [xiter(iter([1, 2, 3, 4]), -1)])

    def test_new_class_list(self):
        a = list([5, 6, 7, 8])
        b = shifty([1, 2,
                    3, 4])
        assert b is not a
        assert isinstance(a, list)
        assert a == [5, 6, 7, 8]

    def test_new_class_list(self):
        b = shifty([1, 2, 3, 4])
        c = new_class(b)
        assert c is not b
        assert isinstance(b, shifty)
        self.assertEquals(b, [1, 2, 3, 4])

    def test_new_class_list_clear(self):
        a = list([5, 6, 7, 8])
        b = new_class([1, 2, 3, 4], clear=True)
        assert b is not a
        assert isinstance(a, list)
        self.assertEquals(a, [])

    def test_new_class_list_clear(self):
        b = shifty([1, 2, 3, 4])
        c = new_class(b, clear=True)
        assert c is not b
        assert isinstance(c, shifty)
        self.assertEquals(c, [])

    def test_clear_instance_shifty_default(self):
        b = shifty([1, 2, 3, 4])
        result = clear_instance(b)
        assert result is b

    def test_clear_instance_shifty_True(self):
        b = shifty([1, 2, 3, 4])
        result = clear_instance(b, clear=True)
        assert result is b
        assert len(b) == 0

    def test_clear_instance_list_default(self):
        b = [1, 2, 3, 4]
        result = clear_instance(b)
        assert result is b

    def test_clear_instance_list_True(self):
        b = [1, 2, 3, 4]
        result = clear_instance(b, clear=True)
        assert result is b
        assert len(b) == 0

    def test_clear_instance_list_shifty(self):
        b = [1, 2, 3, 4]
        result = clear_instance(b, clear=shifty([6, 7, 8, 9]))
        assert result is b
        assert len(b) == 4
        assert b == [6, 7, 8, 9]

    def test_clear_instance_list_list(self):
        b = [1, 2, 3, 4]
        result = clear_instance(b, clear=[6, 7, 8, 9])
        assert result is b
        assert len(b) == 4
        assert b == [6, 7, 8, 9]

    def test_unhandled_type(self):
        self.assertRaises(ValueError, lambda: clear_instance('boo'))

if __name__ == '__main__':
    unittest.main()
