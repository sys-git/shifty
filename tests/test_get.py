# -*- coding: utf-8 -*-
"""
Test shifty shift and wrap.
"""

import unittest

from six.moves import range
from six import Iterator, advance_iterator
from shifty.core import _get_item, _iget_items
from shifty.impls import shifty, _shifter
from shifty.wrappers import get, iget, xget, ixget


class test_get(unittest.TestCase):
    def setUp(self):
        self.a = [1, 2, 3, 4]

    def test_non_shifty_result(self):
        def func(action, a, *args, **kwargs):
            return a

        f = _shifter(func)
        result = f('action', self.a)
        assert result is self.a
        assert not isinstance(result, shifty)

    def test_shifty_result(self):
        def func(action, a, *args, **kwargs):
            return shifty(a)

        f = _shifter(func)
        result = f('action', self.a)
        assert result is not self.a
        assert isinstance(result, shifty)

    def test_get_same_as_iget_list(self):
        a = shifty(self.a)
        result = a.get([0, 1, 2, 3])
        assert result == [1, 2, 3, 4]
        assert isinstance(result, shifty)

    def test_get(self):
        a = shifty(self.a)
        indexes = [2]
        result = []
        iterator = iget(a, indexes)
        for i in range(4):
            try:
                value = advance_iterator(iterator)
            except StopIteration:
                break
            result.append(value)

        result1 = list(a.iget(indexes))
        assert get(a, indexes) == result == result1

    def test_xget(self):
        a = shifty(self.a)
        indexes = [1, 2]
        result = []
        iterator = ixget(a, indexes)
        for i in range(len(indexes)):
            try:
                value = advance_iterator(iterator)
            except StopIteration:
                break
            result.append(value)
        result1 = a.xget(indexes, end=3)
        result2 = list(a.ixget(indexes, end=3))
        assert xget(a, indexes, end=3) == result == result1 == result2

    def test_no_iterable(self):
        self.assertRaises(ValueError, lambda: get(None, []))

    def test_no_indexes(self):
        for v in [None, []]:
            result = get(self.a, v)
            assert isinstance(result, list)
            assert len(result) == 0

    def test_indexes_not_specified_properly(self):
        for v in ['blah', object()]:
            self.assertRaises(ValueError, lambda: get(self.a, v))

    def test_get_item_IndexError_replaced_by_default_value(self):
        a = [1, 2, 3, 4]
        default_value = 'voo'
        value = _get_item(a, 4, default_value=default_value)
        assert value == default_value

    def test_get_item_IndexError_raised(self):
        a = [1, 2, 3, 4]
        self.assertRaises(IndexError,
                          lambda: _get_item(a, 4))

    def test_iget_items_default_item_iter_exception_handled(self):
        a = [1, 2, 3, 4]
        indexes = iter([5])

        class di(Iterator):
            def next(self):
                raise Exception('bang!')

        default_item = di()
        self.assertRaises(IndexError,
                          lambda: list(_iget_items(a, indexes, default_item)))


if __name__ == '__main__':  # pragma no cover
    pass

