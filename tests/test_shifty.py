# -*- coding: utf-8 -*-
"""
Test shifty shift and wrap.
"""

import unittest

from shifty import DEFAULT_VALUE, LEFT, RIGHT, SHIFT, WRAP
from shifty.wrappers import shift, wrap
from shifty.caller import Caller, Iter, CallerData
from shifty.impls import shifty, _shifter
from shifty.action import action_
from shifty.core import _determine_count, _empty_list, _set_items
from shifty.errors import CriticalError, CircularRef
from six.moves import range


class test_shift_enums(unittest.TestCase):
    def test_left(self):
        assert LEFT == 'left'

    def test_right(self):
        assert RIGHT == 'right'

    def test_shift(self):
        assert SHIFT == 'shift'

    def test_wrap(self):
        assert WRAP == 'wrap'


class test_shift_decorators_non_shifty_input(unittest.TestCase):
    def setUp(self):
        self.a = [1, 2, 3, 4]

    def test_non_shifty_result(self):
        def func(action, a, *args, **kwargs):
            return a

        f = _shifter(func)
        result = f('action', self.a)
        assert result is self.a
        assert not isinstance(result, shifty)

    def test_shifty_result(self):
        def func(action, a, *args, **kwargs):
            return shifty(a)

        f = _shifter(func)
        result = f('action', self.a)
        assert result is not self.a
        assert isinstance(result, shifty)


class test_shift_decorators_shifty_input(unittest.TestCase):
    def setUp(self):
        self.a = shifty([1, 2, 3, 4])

    def test_non_shifty_result(self):
        def func(action, a, *args, **kwargs):
            return self.a

        f = _shifter(func)
        result = f('action', self.a)
        assert result is self.a
        assert isinstance(result, shifty)
        assert result.default_value is None

    def test_shifty_result(self):
        def func(action, a, *args, **kwargs):
            return shifty(a)

        f = _shifter(func)
        result = f('action', self.a)
        assert result is not self.a
        assert isinstance(result, shifty)
        assert result.default_value is None

    def test_default_value_propagated(self):
        def func(action, a, *args, **kwargs):
            return self.a

        f = _shifter(func)
        self.a.default_value = [999]
        result = f('action', self.a)
        assert result is self.a
        assert isinstance(result, shifty)
        assert result.default_value == [999], result.default_value


class test_action_bad_values(unittest.TestCase):
    def test_invalid_count(self):
        for i in ['1', float(1), float(1.0)]:
            self.assertRaises(ValueError, lambda: action_(SHIFT, [1, 2, 3, 4],
                                                          i, True, LEFT))

    def test_invalid_action(self):
        for i in [SHIFT + '1', WRAP + '1', '', 1, float(1)]:
            self.assertRaises(ValueError, lambda: action_(i, [1, 2, 3, 4],
                                                          1, True, LEFT))

    def test_invalid_direction(self):
        for i in [LEFT + '1', RIGHT + '1', '0', '', 1, float(1)]:
            self.assertRaises(ValueError, lambda: action_(SHIFT, [1, 2, 3, 4],
                                                          1, True, i))


class test_shifty(unittest.TestCase):
    def test_action(self):
        a = shifty([1, 2, 3, 4])
        assert a.action(WRAP, 1, False, LEFT) == action_(WRAP, a, 1, False,
                                                         LEFT)

    def test_shift(self):
        a = shifty([1, 2, 3, 4])
        assert a.shift(1, False, LEFT) == shift(a, 1, False, LEFT)

    def test_shift_left(self):
        a = shifty([1, 2, 3, 4])
        assert a.shift_left(1, False) == shift(a, 1, False, LEFT) == a.sl(1,
                                                                          False)

    def test_shift_right(self):
        a = shifty([1, 2, 3, 4])
        assert a.shift_right(1, False) == shift(a, 1, False, RIGHT) == a.sr(1,
                                                                            False)

    def test_wrap(self):
        a = shifty([1, 2, 3, 4])
        assert a.wrap(1, False, LEFT) == wrap(a, 1, False, LEFT)

    def test_wrap_left(self):
        a = shifty([1, 2, 3, 4])
        assert a.wrap_left(1, False) == wrap(a, 1, False, LEFT) == a.wl(1,
                                                                        False)

    def test_wrap_right(self):
        a = shifty([1, 2, 3, 4])
        assert a.wrap_right(1, False) == wrap(a, 1, False, RIGHT) == a.wr(1,
                                                                          False)


class test_determine_count(unittest.TestCase):
    def test_unsupported_action(self):
        for count in [-1, 0, 1, float(1), float(-1)]:
            assert _determine_count('blah', count, [1, 2, 3, 4]) == abs(count)

    def test_shift(self):
        for count, a, e_value in [(0, [1, 2, 3, 4], 0),
                                  (-0, [1, 2, 3, 4], 0),
                                  (1, [1, 2, 3, 4], 1),
                                  (-1, [1, 2, 3, 4], 1),
                                  (2, [1, 2, 3, 4], 2),
                                  (-2, [1, 2, 3, 4], 2),
                                  (3, [1, 2, 3, 4], 3),
                                  (-3, [1, 2, 3, 4], 3),
                                  (4, [1, 2, 3, 4], 4),
                                  (-4, [1, 2, 3, 4], 4),
                                  (5, [1, 2, 3, 4], 4),
                                  (-5, [1, 2, 3, 4], 4),
                                  (6, [1, 2, 3, 4], 4),
                                  (-6, [1, 2, 3, 4], 4),
                                  (7, [1, 2, 3, 4], 4),
                                  (-7, [1, 2, 3, 4], 4),
                                  (8, [1, 2, 3, 4], 4),
                                  (-8, [1, 2, 3, 4], 4),
        ]:
            result = _determine_count(SHIFT, count, a)
            assert result == e_value, count

    def test_wrap(self):
        for count, a, e_value in [(0, [1, 2, 3, 4], 0),
                                  (-0, [1, 2, 3, 4], 0),
                                  (1, [1, 2, 3, 4], 1),
                                  (-1, [1, 2, 3, 4], 1),
                                  (2, [1, 2, 3, 4], 2),
                                  (-2, [1, 2, 3, 4], 2),
                                  (3, [1, 2, 3, 4], 3),
                                  (-3, [1, 2, 3, 4], 3),
                                  (4, [1, 2, 3, 4], 0),
                                  (-4, [1, 2, 3, 4], 0),
                                  (5, [1, 2, 3, 4], 1),
                                  (-5, [1, 2, 3, 4], 1),
                                  (6, [1, 2, 3, 4], 2),
                                  (-6, [1, 2, 3, 4], 2),
                                  (7, [1, 2, 3, 4], 3),
                                  (-7, [1, 2, 3, 4], 3),
                                  (8, [1, 2, 3, 4], 0),
                                  (-8, [1, 2, 3, 4], 0),
        ]:
            result = _determine_count(WRAP, count, a)
            assert result == e_value


class test_default_values(unittest.TestCase):
    def test_empty_list(self):
        assert _empty_list(3, None) == [None, None, None]
        assert _empty_list(3, 'str') == ['str', 'str', 'str']


class test_creation(unittest.TestCase):
    def test(self):
        a = shifty([1, 2, 3, 4])
        assert a.default_value == DEFAULT_VALUE

    def test_reset(self):
        a = shifty([1, 2, 3, 4])
        assert a.default_value == DEFAULT_VALUE
        e_value = 'blah'
        a.default_value = e_value
        assert a.default_value == e_value
        a.reset()
        assert a.default_value == DEFAULT_VALUE


class test_CallerData(unittest.TestCase):
    def test_creation(self):
        c = Caller(None)
        assert c.func is None

    def test_creation_with_non_callable(self):
        f = 'blah'
        c = Caller(f)
        assert c.func == f
        assert not c.is_callable
        assert c.callable
        assert not c.iterable

    def test_creation_with_callable(self):
        f = lambda: True
        c = Caller(f)
        assert c.func == f
        assert c.is_callable
        assert c.callable
        assert not c.iterable

    def test_creation_with_iter(self):
        f = lambda: True
        c = Iter(f)
        assert c.func == f
        assert c.is_callable
        assert not c.callable
        assert c.iterable


class test_Caller(unittest.TestCase):
    def test_creation_with_non_callable(self):
        f = 'blah'
        c = Caller(f)
        assert c.func == f
        assert not c.is_callable

    def test_creation_with_callable(self):
        f = lambda: True
        c = Caller(f)
        assert c.func == f
        assert c.is_callable

    def test_get_non_callable(self):
        # should return item:
        item = 'blah'
        assert CallerData.get_value(item) == item

    def test_get_callable(self):
        # should return item:
        def func():
            return 123

        assert CallerData.get_value(func) == func

    def test_get_Caller_non_callable(self):
        # should return item:
        e_value = 123
        item = Caller(e_value)
        assert CallerData.get_value(item) == e_value

    def test_repr(self):
        item = Caller(123)
        result = repr(item)
        assert result == 'User-Callable(123)'

    def test_neither_iterable_or_callable(self):
        item = CallerData(123)
        self.assertRaises(CriticalError, lambda: item.get_value(item))


class test_Caller_getCallerData(unittest.TestCase):
    def test_get_Caller_callable(self):
        # should evaluate item:
        e_value = 456
        item = Caller(lambda: e_value)
        self.assertEquals(CallerData.get_value(item), e_value)
        self.assertEquals(item.value, e_value)

    def test_get_Caller_Caller_callable(self):
        # should evaluate item:
        e_value = 456
        item = Caller(Caller(lambda: e_value))
        self.assertEquals(CallerData.get_value(item), e_value)
        self.assertEquals(item.value, e_value)

    def test_get_Caller_Caller_non_callable(self):
        # should not evaluate item:
        e_value = 789
        item = Caller(Caller(e_value))
        self.assertEquals(CallerData.get_value(item), e_value)
        self.assertEquals(item.value, e_value)

    def test_get_Caller_Iter_callable(self):
        # should evaluate iterable item:
        e_value = list(range(3))
        item = Caller(Iter(e_value))
        self.assertEquals(CallerData.get_value(item), e_value)
        self.assertEquals(item.value, e_value)


class test_Iter(unittest.TestCase):
    def test_creation(self):
        c = Iter(None)
        assert c.func is None

    def test_creation_with_non_callable(self):
        f = 'blah'
        c = Iter(f)
        assert c.func == f
        assert not c.is_callable

    def test_creation_with_callable(self):
        f = lambda: True
        c = Iter(f)
        assert c.func == f
        assert c.is_callable

    def test_get_Iter_non_iterable(self):
        item = Iter(123)
        self.assertRaises(TypeError, lambda: CallerData.get_value(item))

    def test_get_Iter_iterable(self):
        # should evaluate item:
        e_value = [1, 2, 3]
        item = Iter(e_value)
        b = CallerData.get_value(item)
        self.assertEquals(item.value, e_value)
        assert b is not e_value
        assert b == e_value

    def test_get_Iter_Iter_callable(self):
        item = Iter(Iter(lambda: 765))
        self.assertRaises(TypeError, lambda: CallerData.get_value(item))

    def test_get_Iter_Iter_iterable(self):
        a = [1, 2, 3]
        b = [4, 5, 6]
        item = Iter(Iter([a, b]))
        c = CallerData.get_value(item)
        e_value = [a, b]
        self.assertEquals(item.value, e_value)
        assert a is not c
        assert c == e_value

    def test_get_Iter_iterable_Caller_callable(self):
        # should evaluate iterable item:
        item = Caller(Iter(range(3)))
        e_value = [0, 1, 2]
        self.assertEquals(CallerData.get_value(item), e_value)
        self.assertEquals(item.value, e_value)

    def test_get_Iter_of_all_depth_one(self):
        # should evaluate iterable item:
        item = Iter([123, Caller(lambda: 456), Caller(789), Iter('xyz')])
        e_value = [123, 456, 789, ['x', 'y', 'z']]
        self.assertEquals(CallerData.get_value(item), e_value)
        self.assertEquals(item.value, e_value)

    def test_get_Iter_of_all_depth_three(self):
        # should evaluate iterable item:
        def func():
            pass

        item = Iter([
            Iter([123, Caller(lambda: 456), Caller(789), Iter('xyz'),
                  Iter(['q', Caller('boo'), Iter('meow'), 'z'])]),
            Iter([111, func, Caller(Caller(lambda: 444)), Caller(Caller(555)),
                  Iter('xxx')]),
            Iter(range(3)),
            Caller('abc'),
            Caller(lambda: 234),
            Caller(Iter(range(5))),
            Caller(Caller('boo')),
            Caller(Caller(lambda: 654)),
        ])

        e_value = [
            [123, 456, 789, ['x', 'y', 'z'],
             ['q', 'boo', ['m', 'e', 'o', 'w'], 'z']],
            [111, func, 444, 555, ['x', 'x', 'x']],
            list(range(3)),
            'abc',
            234,
            list(range(5)),
            'boo',
            654,
        ]
        self.assertEquals(CallerData.get_value(item), e_value)
        self.assertEquals(item.value, e_value)

    def test_repr(self):
        item = Iter(123)
        result = repr(item)
        assert result == 'Iter(123)'


class test_get_recursion(unittest.TestCase):
    def test_Iter(self):
        a = Iter([123])
        b = Iter(a)
        a._func = b
        self.assertRaises(CircularRef, lambda: b.value)


class test_private_set_items(unittest.TestCase):
    def test_items_exhausted(self):
        a = [1, 2, 3, 4]
        _set_items(iter(a), iter([0]), iter([]))
        assert a == [1, 2, 3, 4]

    def test_iindexes_exhausted(self):
        a = [1, 2, 3, 4]
        _set_items(iter(a), iter([]), iter([0]))
        assert a == [1, 2, 3, 4]


class test_clear(unittest.TestCase):
    def test_items_exhausted(self):
        a = shifty([1, 2, 3, 4])
        b = a.clear()
        assert a == []
        assert a is b


if __name__ == '__main__':  # pragma no cover
    pass

