# -*- coding: utf-8 -*-
"""
Test shifty: shift.
"""

import unittest

from shifty.wrappers import set_items, clear_items, xset_items, xclear_items
from shifty.impls import shifty
from shifty.utils import get_class
from shifty.action import set_items as set_items_
from shifty.action import clear_items as clear_items_
from shifty.interfaces import base_api


class test_get_class(unittest.TestCase):
    def setUp(self, in_place=True):
        self.a = shifty([1, 2, 3, 4])
        self.in_place = in_place

    def test_shifty(self):
        result = get_class(self.a)
        assert result == shifty

    def test_list(self):
        a = [1, 2, 3]
        result = get_class(a)
        assert result is not a
        assert result is list

    def test_base_api(self):
        class Voo(base_api):
            def clear(self):
                pass

            def reset(self):
                pass

            @property
            def default_value(self):
                pass

            @default_value.setter
            def default_value(self, value):
                pass

            def action(self):
                pass

        result = get_class(Voo())()
        assert isinstance(result, Voo)

    def test_other(self):
        self.assertRaises(ValueError, lambda: get_class('123'))


class test_set_items(unittest.TestCase):
    def setUp(self):
        self.a = shifty([1, 2, 3, 4])
        self.b = shifty([1, 2, 3, 4])

    def test_no_iterable(self):
        self.assertRaises(ValueError, lambda: set_items(None, [1], [1]))

    def test_no_items_in_place(self):
        assert set_items(self.a, [], [1], in_place=True) is self.a
        assert self.a == [1, 2, 3, 4]
        assert self.b.set_items([], [1], in_place=True) is self.b

    def test_no_items_no_indexes(self):
        assert set_items(self.a, [], [], in_place=True) is self.a
        assert self.b.set_items([], [], in_place=True) is self.b

    def test_single_item_no_indexes_in_place(self):
        assert set_items(self.a, 'blah', [], in_place=True) is self.a
        assert self.b.set_items('blah', [], in_place=True) is self.b

    def test_single_item_no_indexes_not_in_place(self):
        result = set_items(self.a, 'blah', [], in_place=False)
        assert result is not self.a
        assert result == [1, 2, 3, 4]
        result = self.b.set_items('blah', [], in_place=False)
        assert result is not self.b
        assert result == [1, 2, 3, 4]

    def test_single_item_indexes_in_place(self):
        result = set_items(self.a, 'blah', [0], in_place=True)
        assert result is self.a
        assert result == ['blah', 2, 3, 4]
        assert self.b.set_items('blah', [0], in_place=True) is self.b
        assert self.b == ['blah', 2, 3, 4]

    def test_single_item_indexes_not_in_place(self):
        result = set_items(self.a, 'blah', [0], in_place=False)
        assert result is not self.a
        assert result == ['blah', 2, 3, 4]
        result = self.b.set_items('blah', [0], in_place=False)
        assert result is not self.b
        assert result == ['blah', 2, 3, 4]

    def test_invalid_action(self):
        self.assertRaises(ValueError,
                          lambda: set_items_('blah', None, None, None))


class test_set_items_with_default_value(unittest.TestCase):
    def setUp(self):
        self.a = shifty([1, 2, 3, 4])

    def test_no_items_no_indexes_with_default_value_in_place(self):
        assert set_items(self.a, [], [1], in_place=True,
                         default_value='p') is self.a
        assert self.a == [1, 'p', 3, 4]

        assert set_items(self.a, None, [1], in_place=True,
                         default_value='p') is self.a
        assert self.a == [1, 'p', 3, 4]

        self.assertRaises(ValueError,
                          lambda: set_items(self.a, None, float(1.2),
                                            in_place=True, default_value='p'))

        # This should work:
        assert set_items(self.a, None, 3, in_place=True,
                         default_value='q') is self.a
        assert self.a == [1, 'p', 3, 'q']

    def test_no_items_no_indexes_with_default_value_not_in_place(self):
        result = set_items(self.a, [], [1], in_place=False,
                           default_value='p')
        assert result is not self.a
        assert result == [1, 'p', 3, 4]
        result = set_items(self.a, None, [1], in_place=False,
                           default_value='p')
        assert result is not self.a
        assert result == [1, 'p', 3, 4]

        self.assertRaises(ValueError,
                          lambda: set_items(self.a, None, float(1.2),
                                            in_place=True, default_value='p'))

        # This should work:
        result = set_items(self.a, None, 3, in_place=False,
                           default_value='q')
        assert result is not self.a
        assert result == [1, 2, 3, 'q']

    def test_not_list_iterable_becomes_list(self):
        assert set_items(self.a, [], (1, 2), in_place=True,
                         default_value='p') is self.a
        assert self.a == [1, 'p', 'p', 4]


class test_set_items_with_no_default_value(unittest.TestCase):
    def setUp(self):
        self.a = shifty([1, 2, 3, 4])

    def test_no_items_no_indexes_with_default_value_in_place(self):
        assert set_items(self.a, [], [1], in_place=True) is self.a
        self.assertEquals(self.a, [1, 2, 3, 4])

        assert set_items(self.a, None, [1], in_place=True) is self.a
        assert self.a == [1, 2, 3, 4]

        self.assertRaises(ValueError,
                          lambda: set_items(self.a, None, float(1.2),
                                            in_place=True))

        # This should work:
        assert set_items(self.a, None, 3, in_place=True) is self.a
        assert self.a == [1, 2, 3, 4]

    def test_no_items_no_indexes_with_default_value_not_in_place(self):
        result = set_items(self.a, [], [1], in_place=False)
        assert result is not self.a
        assert result == [1, 2, 3, 4]
        result = set_items(self.a, None, [1], in_place=False)
        assert result is not self.a
        assert result == [1, 2, 3, 4]

        self.assertRaises(ValueError,
                          lambda: set_items(self.a, None, float(1.2),
                                            in_place=True))

        # This should work:
        result = set_items(self.a, None, 3, in_place=False)
        assert result is not self.a
        assert result == [1, 2, 3, 4]

    def test_not_list_iterable_becomes_list(self):
        assert set_items(self.a, [], (1, 2), in_place=True) is self.a
        assert self.a == [1, 2, 3, 4]


class test_clear_items(unittest.TestCase):
    def setUp(self):
        self.a = shifty([1, 2, 3, 4])
        self.b = shifty([1, 2, 3, 4])

    def test_no_iterable(self):
        self.assertRaises(ValueError,
                          lambda: clear_items(None, [1], in_place=True))

    def test_no_indexes(self):
        assert clear_items(self.a, [], in_place=True) is self.a
        result = self.b.clear_items([], in_place=True)
        assert result is self.b
        self.assertEqual(result, [1, 2, 3, 4])

    def test_no_indexes(self):
        assert clear_items(self.a, [], in_place=False) is not self.a
        result = self.b.clear_items([], in_place=False)
        assert result is not self.b
        self.assertEqual(result, [1, 2, 3, 4])

    def test_invalid_action(self):
        self.assertRaises(ValueError,
                          lambda: clear_items_('blah', None, None))

    def test_index_is_invalid(self):
        indexes = float(1.2)
        self.assertRaises(ValueError, lambda: clear_items(self.a, indexes))


class test_clear_items_with_default_value(unittest.TestCase):
    def setUp(self):
        self.a = shifty([1, 2, 3, 4])

    def test_not_list_iterable_becomes_list(self):
        assert clear_items(self.a, (1, 2), in_place=True,
                           default_value='p') is self.a
        assert self.a == [1, 'p', 'p', 4]

    def test_index_is_list(self):
        indexes = [1, 2]
        result = clear_items(self.a, indexes, in_place=False, default_value='p')
        assert result is not self.a
        assert result == [1, 'p', 'p', 4]

    def test_index_is_list_in_place(self):
        indexes = [1, 2]
        result = clear_items(self.a, indexes, in_place=True, default_value='p')
        assert result is self.a
        assert result == [1, 'p', 'p', 4]

    def test_index_is_tuple(self):
        indexes = (1, 2)
        result = clear_items(self.a, indexes, in_place=False, default_value='p')
        assert result is not self.a
        assert result == [1, 'p', 'p', 4]

    def test_index_is_tuple_in_place(self):
        indexes = (1, 2)
        result = clear_items(self.a, indexes, in_place=True, default_value='p')
        assert result is self.a
        assert result == [1, 'p', 'p', 4]

    def test_index_is_int(self):
        indexes = 1
        result = clear_items(self.a, indexes, in_place=False, default_value='p')
        assert result is not self.a
        assert result == [1, 'p', 3, 4]

    def test_index_is_int_in_place(self):
        indexes = 1
        result = clear_items(self.a, indexes, in_place=True, default_value='p')
        assert result is self.a
        assert result == [1, 'p', 3, 4]


class test_clear_items_with_no_default_value(unittest.TestCase):
    def setUp(self):
        self.a = shifty([1, 2, 3, 4])

    def test_not_list_iterable_becomes_list(self):
        assert clear_items(self.a, (1, 2), in_place=True) is self.a
        assert self.a == [1, None, None, 4]

    def test_index_is_list(self):
        indexes = [1, 2]
        result = clear_items(self.a, indexes, in_place=False)
        assert result is not self.a
        assert result == [1, None, None, 4]

    def test_index_is_list_in_place(self):
        indexes = [1, 2]
        result = clear_items(self.a, indexes, in_place=True)
        assert result is self.a
        assert result == [1, None, None, 4]

    def test_index_is_tuple(self):
        indexes = (1, 2)
        result = clear_items(self.a, indexes, in_place=False)
        assert result is not self.a
        assert result == [1, None, None, 4]

    def test_index_is_tuple_in_place(self):
        indexes = (1, 2)
        result = clear_items(self.a, indexes, in_place=True)
        assert result is self.a
        assert result == [1, None, None, 4]

    def test_index_is_int(self):
        indexes = 1
        result = clear_items(self.a, indexes, in_place=False)
        assert result is not self.a
        assert result == [1, None, 3, 4]

    def test_index_is_int_in_place(self):
        indexes = 1
        result = clear_items(self.a, indexes, in_place=True)
        assert result is self.a
        assert result == [1, None, 3, 4]

    def test_xset_items(self):
        a = shifty([1, 2, 3, 4])
        items = [5, 6, 7]
        iterable = [1, 2, 3, 4]
        iterable1 = [1, 2, 3, 4]
        indexes = [2, 3]
        try:
            xset_items(iterable, items, indexes, in_place=True)
        except IndexError:
            pass
        try:
            set_items_(iterable1,
                       items,
                       indexes,
                       in_place=True,
                       invert=True)
        except IndexError:
            pass

        try:
            a.xset_items(indexes, in_place=True)
        except IndexError:
            pass

        try:
            a.xset_items(items, indexes, in_place=True)
        except IndexError:
            pass
        assert iterable == iterable1 == a

    def test_xclear_items(self):
        a = shifty([1, 2, 3, 4])
        iterable = [1, 2, 3, 4]
        iterable1 = [1, 2, 3, 4]
        indexes = [2, 3]
        try:
            xclear_items(iterable, indexes, in_place=True)
        except IndexError:
            pass
        try:
            clear_items_(iterable1,
                         indexes,
                         in_place=True,
                         invert=True)
        except IndexError:
            pass

        try:
            a.xclear_items(indexes, in_place=True)
        except IndexError:
            pass

        assert iterable == iterable1 == a


if __name__ == '__main__':  # pragma no cover
    unittest.main()

