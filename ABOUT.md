# README #

A pure-python list-shifter/wrapper.
Shifts/Wraps an iterable left or right by any amount (modulo to the length of 
iterable).
Can optionally shift the iterable in-place.
Create a shifty instance to add these convenience methods to the default list
 implementation if you prefer.

Why slice when you can shift 'like-a-boss' ?!

[ ![Codeship Status for sys-git/shifty](https://codeship.com/projects/3b9b4360-9a44-0132-7e10-0ee228cf83fe/status?branch=master)](https://codeship.com/projects/63870)
[![Build Status](https://api.shippable.com/projects/54afbe1ad46935d5fbc1e905/badge?branchName=master)](https://app.shippable.com/projects/54afbe1ad46935d5fbc1e905/builds/latest)

[![Downloads](https://pypip.in/download/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![Latest Version](https://pypip.in/version/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![Supported Python versions](https://pypip.in/py_versions/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![Supported Python implementations](https://pypip.in/implementation/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![Development Status](https://pypip.in/status/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![Wheel Status](https://pypip.in/wheel/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![Egg Status](https://pypip.in/egg/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![Download format](https://pypip.in/format/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)
[![License](https://pypip.in/license/shifty/badge.svg)](https://pypi.python.org/pypi/shifty/)


### How do I get set up? ###

* **python setup.py install**
* Dependencies: **None**
* Dependencies (test):  **Coverage, nose**
* How to run tests:  **./runtests.sh**
* Deployment instructions:  **pip install shifty**

### Contribution guidelines ###
I accept pull requests.

### What about test coverage? ###
There is a full suite of unit-tests.

### Who do I talk to? ###

* Francis Horsman:  **francis.horsman@gmail.com**

### Example ###

```
>>> from shifty import shift_left, shifty

>>> a = [1, 2, 3, 4]
>>> b = shift_left(a, 3)
>>> b
[4, 1, 2, 3]
>>> assert a is not b

>>> b = shift_left(a, 2, in_place=True)

>> assert a is b

>>> b = shift_left(a, 5)

>>> b
[4, 1, 2, 3]

```